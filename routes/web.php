<?php

use Illuminate\Support\Facades\Route;

// //Halaman Login
Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    
    //Halaman Home
    Route::get('/','HomeController@index');

    //Halaman Master Kategori MAP (Mata Anggaran Penerimaan)
    Route::get('map_category/data','MapCategoryController@json');
    Route::Resource('map_category','MapCategoryController');

    //Halaman Master MAP (Mata Anggaran Penerimaan)
    Route::get('/map/data','MapController@json');
    Route::get('/map/check_map_id_exist','MapController@checkMapIdExist');
    Route::Resource('map','MapController');

    //Halaman Data Time Series
    //Halaman Input Data Time Series
    Route::get('/time_series/form','TimeSeriesController@form');
    Route::get('/time_series/download_format','TimeSeriesController@downloadFormat');
    Route::post('/time_series/load_excel','TimeSeriesController@loadExcel');
    Route::post('/time_series/store','TimeSeriesController@store');
    //Halaman Untuk Menampilkan Data Time Series
    Route::get('/time_series/data','TimeSeriesController@allTimeSeriesData');
    Route::get('/time_series/annual_data/{year}','TimeSeriesController@annualData');
    //Edit & Delete Data Time Series
    Route::get('/time_series/edit','TimeSeriesController@edit');
    Route::post('/time_series/update','TimeSeriesController@update');
    Route::post('/time_series/delete','TimeSeriesController@delete');
    //Plot Data Time Series
    Route::get('/time_series/plot_data','TimeSeriesController@plotData');
    Route::get('/time_series/get_plot_data','TimeSeriesController@getPlotData');

    //Halaman Foreacasting
    Route::get('/forecasting','ForecastingController@index');
    Route::get('/forecasting/calculate_forecasts','ForecastingController@calculateForecasts');

    //User
    Route::get('/user/data','UserController@json');
    Route::get('/user','UserController@index');
    Route::post('/user/store','UserController@store');
    Route::get('/user/edit/{id}','UserController@edit');
    Route::post('/user/update','UserController@update');
    Route::post('/user/update_password','UserController@updatePassword');
    Route::post('/user/delete','UserController@delete');
});
