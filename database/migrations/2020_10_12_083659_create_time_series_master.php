<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeSeriesMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_series_master', function (Blueprint $table) {
            $table->id();
            $table->string('map_id',6);
            $table->foreign('map_id')->references('map_id')->on('master_map')->onUpdate('cascade')->onDelete('restrict');
            $table->year('year');
            $table->text('description');            
            $table->integer('jan', false, true)->length(15)->unsigned(TRUE);
            $table->integer('feb', false, true)->length(15)->unsigned(TRUE);
            $table->integer('mar', false, true)->length(15)->unsigned(TRUE);
            $table->integer('apr', false, true)->length(15)->unsigned(TRUE);
            $table->integer('may', false, true)->length(15)->unsigned(TRUE);
            $table->integer('jun', false, true)->length(15)->unsigned(TRUE);
            $table->integer('jul', false, true)->length(15)->unsigned(TRUE);
            $table->integer('aug', false, true)->length(15)->unsigned(TRUE);
            $table->integer('sep', false, true)->length(15)->unsigned(TRUE);
            $table->integer('oct', false, true)->length(15)->unsigned(TRUE);
            $table->integer('nov', false, true)->length(15)->unsigned(TRUE);
            $table->integer('dec', false, true)->length(15)->unsigned(TRUE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_series_master');
    }
}
