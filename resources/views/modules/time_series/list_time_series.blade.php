@extends('master_template')

@section('konten')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0"><b>List Time Series</b></h5>
                        <div class="float-right">
                            <a href="{{ url('/') }}">Home</a> / Time Series / List Data
                        </div>
                    </div>
                    <div class="card-body">                        
                        <div id="alert_" class="alert alert-info alert-dismissible fade show" role="alert">
                            <b id="alert_messages_">&nbsp;</b>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-3">
                                <small class='font-weight-bold'>Tahun</small>   
                                <select name="year" id="year" class="form-control form-control-sm" onchange="loadTable()">
                                    @foreach($year_list as $data)
                                    <option value="{{$data->year}}">{{$data->year}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div id="place_of_data"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Modal -->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form name="form_input" method="POST">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel"><b>Edit Data Time Series</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" id="id">
                <div class="row">
                    <div class="col-md-6">
                        <small class='font-weight-bold'>MAP</small>
                        <select name="map_id" id="map_id" class="form-control form-control-sm" required>
                            @foreach($map_data as $map)
                            <option value="{{$map->map_id}}">{{$map->map_name}}</option>
                            @endforeach
                        </select>
                        <!-- <input type="text" class="form-control form-control-sm" name="map" id="map" /> -->
                    </div>
                    <div class="col-md-6">
                        <small class='font-weight-bold'>Tahun</small>   
                        <select name="year" id="year" class="form-control form-control-sm" onchange="loadTable()">
                            @foreach($year_list as $data)
                            <option value="{{$data->year}}" >{{$data->year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">
                        <small class='font-weight-bold'>Keterangan</small>   
                        <textarea class="form-control form-control-sm" name="description" id="description"></textarea>
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Jan</small>   
                        <input type="text" name="jan" id="jan" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Feb</small>   
                        <input type="text" name="feb" id="feb" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Mar</small>   
                        <input type="text" name="mar" id="mar" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Apr</small>   
                        <input type="text" name="apr" id="apr" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Mei</small>   
                        <input type="text" name="may" id="may" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Jun</small>   
                        <input type="text" name="jun" id="jun" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Jul</small>   
                        <input type="text" name="jul" id="jul" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Agu</small>   
                        <input type="text" name="aug" id="aug" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Sep</small>   
                        <input type="text" name="sep" id="sep" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Okt</small>   
                        <input type="text" name="oct" id="oct" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Nov</small>   
                        <input type="text" name="nov" id="nov" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-4">
                        <small class='font-weight-bold'>Des</small>   
                        <input type="text" name="dec" id="dec" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" onclick="update()"><i class="fa fa-save"></i> Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('konten_js')
<script>
    //Initialization - Loaded First
    $(function () {
        loadTable();
        $("#alert_").hide();
    });
    
    //CSRF Token - to prevent XSS Attack
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Table
    function loadTable(){
        var year = $("#year :selected").val();
        
        $.ajax({
            type : "GET",
            url : "{{ url('/time_series/annual_data') }}/"+year,
            success : function (data){
                $("#place_of_data").html(data);
            }
        });
    }

    //Edit Data
    function edit(id){
        $.ajax({
            method : "GET",
            url: "{{ url('/time_series/edit')}}",
            data : {
                "id" : id
            },
            type:'json',
            success: function (ajaxData) {
                $("#id").val(ajaxData.id);
                $("#map_id").val(ajaxData.map_id); //Ganti Pake Combobox
                $("#year").val(ajaxData.year);
                $("#description").val(ajaxData.description);
                $("#jan").val(ajaxData.jan);
                $("#feb").val(ajaxData.feb);
                $("#mar").val(ajaxData.mar);
                $("#apr").val(ajaxData.apr);
                $("#may").val(ajaxData.may);
                $("#jun").val(ajaxData.jun);
                $("#jul").val(ajaxData.jul);
                $("#aug").val(ajaxData.aug);
                $("#sep").val(ajaxData.sep);
                $("#oct").val(ajaxData.oct);
                $("#nov").val(ajaxData.nov);
                $("#dec").val(ajaxData.dec);
            },
        });            
        $("#modal_edit").modal('show');
    }

    function update(){
        $.ajax({
            url : "{{ url('/time_series/update') }}",
            method : "POST",
            data : {
                'id' : $("#id").val(),
                'map_id' : $("#map_id :selected").val(),
                'year' : $("#year :selected").val(),
                'description' : $("#description").val(),
                'jan' : $("#jan").val(),
                'feb' : $("#feb").val(),
                'mar' : $("#mar").val(),
                'apr' : $("#apr").val(),
                'may' : $("#may").val(),
                'jun' : $("#jun").val(),
                'jul' : $("#jul").val(),
                'aug' : $("#aug").val(),
                'sep' : $("#sep").val(),
                'oct' : $("#oct").val(),
                'nov' : $("#nov").val(),
                'dec' : $("#dec").val(),
                
            },
            success : function(ajaxData){
                //Hide Modal
                $("#modal_edit").modal('hide');

                //Modifikasi Alert                
                if(ajaxData == "1"){
                    $("#alert_messages_").text('Berhasil');
                }else{
                    $("#alert_messages_").text('Gagal');
                }
                $("#alert_").show();
                //Reload Table
                loadTable();
            }
        });
    }

    function del(dataID){
        if(confirm('Anda Yakin Menghapus Data?')){
            $.ajax({
                url : "{{ url('/time_series/delete') }}",
                method : "POST",
                data : {
                    'id_' : dataID
                },
                success : function(ajaxData){
                    //Modifikasi Alert                
                    if(ajaxData == "1"){
                        $("#alert_messages_").text('Berhasil Menghapus Data');
                    }else{
                        $("#alert_messages_").text('Gagal Menghapus Data');
                    }
                    $("#alert_").show();
                    //Reload Table
                    loadTable();
                }
            });
        }
    }
    

    
  </script>
@endsection