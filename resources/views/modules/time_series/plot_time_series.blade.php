@extends('master_template')

@section('konten')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0"><b>Plot Time Series</b></h5>
                        <div class="float-right">
                            <a href="{{ url('/') }}">Home</a> / Time Series / Plot Data
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-lg-2">
                                <small class='font-weight-bold'>Tipe</small>   
                                <select name="tipe" id="tipe" class="form-control form-control-sm" onchange="loadChart()">
                                    <option value="annual">Tahunan</option>
                                    <option value="monthly">Bulanan</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <small class='font-weight-bold'>Dari</small>   
                                <select name="early_year" id="early_year" class="form-control form-control-sm" onchange="loadChart()">
                                    @foreach($year_list as $data)
                                    <option value="{{$data->year}}">{{$data->year}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <small class='font-weight-bold'>Sampai</small>   
                                <select name="final_year" id="final_year" class="form-control form-control-sm" onchange="loadChart()">
                                    @foreach($year_list as $data)
                                    <option value="{{$data->year}}">{{$data->year}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div id="alert_" class="alert alert-info alert-dismissible fade show" role="alert">
                            <b id="alert_messages_">&nbsp;</b>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="chart">
                            <canvas id="plotChart" style="height:400px"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('konten_js')
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>

<script>
    //Initialization - Loaded First
    $(function () {
        loadChart()
        $("#alert_").hide();
    });
    
    //CSRF Token - to prevent XSS Attack
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    

    //Table
    function loadChart(){
        var tipe = $("#tipe :selected").val()
        var early_year = $("#early_year").val()
        var final_year = $("#final_year").val()
        
        $.ajax({
            type : "GET",
            url : "{{ url('/time_series/get_plot_data') }}",
            data : {tipe : tipe, early_year : early_year, final_year : final_year},
            success : function (data){
                var library_colors = ['#000','#F20231','#8F87D2','#4CCBA6','#0ACDFF','#F8A41D',
                                    '#7C6A0A','#384182','#EFBF7F','#FF9FB2','#78290F'];

                //Looping Dataset
                var dataset = [];
                var data_recapitulation = data.data_recapitulation
                data_recapitulation.forEach(function(result, key){                    

                    var new_data = {
                            label: result.category_name, // Name the series
                            data: result.pnbp_income, // Specify the data values array
                            fill: false,
                            borderColor: library_colors[key], // Add custom color border (Line)
                            backgroundColor: library_colors[key], // Add custom color background (Points and Fill)
                            borderWidth: 1 // Specify bar border width
                    }            
                    dataset.push(new_data);
                });

                createPlotChart(data.nameset,dataset)
                
            }
        });
    }

    function createPlotChart(nameset, dataset){
        var ctx = document.getElementById("plotChart").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: nameset,
                        datasets: dataset
                    },         
                    options: {
                    responsive: true, // Instruct chart js to respond nicely.
                    maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
                    }
                });
    }
    
  </script>
@endsection