<table class="table table-sm table-bordered mt-2"  id="time_series_table">
    <thead>
        <tr>
            <td class="font-weight-bold text-center">Keterangan</td>
            <td class="font-weight-bold text-center">Jan</td>
            <td class="font-weight-bold text-center">Feb</td>
            <td class="font-weight-bold text-center">Mar</td>
            <td class="font-weight-bold text-center">Apr</td>
            <td class="font-weight-bold text-center">Mei</td>
            <td class="font-weight-bold text-center">Jun</td>
            <td class="font-weight-bold text-center">Jul</td>
            <td class="font-weight-bold text-center">Agu</td>
            <td class="font-weight-bold text-center">Sep</td>
            <td class="font-weight-bold text-center">Okt</td>
            <td class="font-weight-bold text-center">Nov</td>
            <td class="font-weight-bold text-center">Des</td>
            <td class="font-weight-bold text-center">Aksi</td>
        </tr>
    </thead>
    <tbody id="place_of_data">
    <!-- Foreach All Data -->
    @foreach($all_data as $data)
        <tr style="background-color:#F2F2F2;">
            <td colspan="14">--{{ $data['map_name'] }} ({{ $data['map_id'] }})--</td>
        </tr>

        <!-- Get Master Data -->
        @foreach($data['time_series_master'] as $ts)
        <tr>
            <td>{{ $ts['description'] }}</td>
            <td class="text-right">{{ number_format($ts['jan'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['feb'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['mar'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['apr'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['may'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['jun'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['jul'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['aug'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['sep'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['oct'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['nov'],0,',','.') }}</td>
            <td class="text-right">{{ number_format($ts['dec'],0,',','.') }}</td>

            <td class="text-center">
                <button class="btn btn-sm btn-primary" onclick="edit({{ $ts['id'] }})"><i class="fa fa-pencil-alt"></i></button>
                <button class="btn btn-sm btn-danger" onclick="del({{ $ts['id'] }})"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
        @endforeach


    @endforeach
    </tbody>
</table>