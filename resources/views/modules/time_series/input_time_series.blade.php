@extends('master_template')

@section('konten')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0"><b>Input Time Series</b></h5>
                        <div class="float-right">
                            <a href="{{ url('/') }}">Home</a> / Input Time Series
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="alert_" class="alert alert-info alert-dismissible fade show" role="alert">
                            <b id="alert_messages_">&nbsp;</b>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" name="upload_form" id="upload_form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-3 ">
                                    <label for="">File Excel</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="excel_file" name="excel_file">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-3 text-right">
                                    <button type="button" id="btnDownload" class="btn btn-sm btn-success">Download Format (.xlsx)</button>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-sm btn-info" id="btnPreviewx" onsubmit="return false">Preview</button>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-2">
                                    <small class="font-weight-bold">Tahun</small>
                                    <input type="text" class="form-control form-control-sm" id="tahun">
                                </div>
                                <div class="col-md-12">
                                    <table class="table table-sm table-striped table-bordered mt-2"  id="table_preview">
                                        <thead>
                                            <tr>
                                                <td class="font-weight-bold text-center">MAP</td>
                                                <td class="font-weight-bold text-center">Keterangan</td>
                                                <td class="font-weight-bold text-center">Jan</td>
                                                <td class="font-weight-bold text-center">Feb</td>
                                                <td class="font-weight-bold text-center">Mar</td>
                                                <td class="font-weight-bold text-center">Apr</td>
                                                <td class="font-weight-bold text-center">Mei</td>
                                                <td class="font-weight-bold text-center">Jun</td>
                                                <td class="font-weight-bold text-center">Jul</td>
                                                <td class="font-weight-bold text-center">Agu</td>
                                                <td class="font-weight-bold text-center">Sep</td>
                                                <td class="font-weight-bold text-center">Okt</td>
                                                <td class="font-weight-bold text-center">Nov</td>
                                                <td class="font-weight-bold text-center">Des</td>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_table_preview">
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                            
                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-sm btn-primary" id="btnSave">Simpan</button>
                                </div>
                            </div>
                        </form >
                            
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('konten_js')
<!-- BS Custom File Input -->
<script src="{{ url('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }} "></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#alert_").hide();
        bsCustomFileInput.init();
    });
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(document).ready(function() {
        $("#btnDownload").on('click',function(event){
            window.open("{{ url('/time_series/download_format') }}?"+ Math.random() , '_blank');
        })

        $('#upload_form').on('submit', function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ url('/time_series/load_excel') }}?"+Math.random(),
                method: 'POST',
                data: new FormData(this),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(ajaxData) {
                    $("#tbody_table_preview").empty()
                    console.log(ajaxData)

                    //Year
                    $("#tahun").val(ajaxData.tahun);

                    //Detail Data
                    //Make Alias to decrease variable name length
                    var data = ajaxData.data_uploaded;
                    for(var i=0;i<data.length;i++){
                        $("#tbody_table_preview").append(
                            `<tr>
                                <td><input type="text" name="map[]" id="map" class="form-control form-control-sm" value="`+data[i].map+`" /></td>
                                <td><input type="text" name="description[]" id="keterangan" class="form-control form-control-sm" value="`+data[i].keterangan+`" /></td>                                
                                <td><input type="number" name="jan[]" id="jan" class="form-control form-control-sm" value="`+data[i].jan+`" /></td>                                
                                <td><input type="number" name="feb[]" id="feb" class="form-control form-control-sm" value="`+data[i].feb+`" /></td>                                
                                <td><input type="number" name="mar[]" id="mar" class="form-control form-control-sm" value="`+data[i].mar+`" /></td>                                
                                <td><input type="number" name="apr[]" id="apr" class="form-control form-control-sm" value="`+data[i].apr+`" /></td>                                
                                <td><input type="number" name="may[]" id="may" class="form-control form-control-sm" value="`+data[i].may+`" /></td>                                
                                <td><input type="number" name="jun[]" id="jun" class="form-control form-control-sm" value="`+data[i].jun+`" /></td>                                
                                <td><input type="number" name="jul[]" id="jul" class="form-control form-control-sm" value="`+data[i].jul+`" /></td>                                
                                <td><input type="number" name="aug[]" id="aug" class="form-control form-control-sm" value="`+data[i].aug+`" /></td>                                
                                <td><input type="number" name="sep[]" id="sep" class="form-control form-control-sm" value="`+data[i].sep+`" /></td>                                
                                <td><input type="number" name="oct[]" id="oct" class="form-control form-control-sm" value="`+data[i].oct+`" /></td>                                
                                <td><input type="number" name="nov[]" id="nov" class="form-control form-control-sm" value="`+data[i].nov+`" /></td>                                
                                <td><input type="number" name="dec[]" id="dec" class="form-control form-control-sm" value="`+data[i].dec+`" /></td>                                
                            </tr>`
                        )                        
                    }                    
                }
            });
        });

        $('#btnSave').on('click', function(event) {
            event.preventDefault();
            
            
            //Cek Dulu MAP nya. Jika Map yang diinputkan Tidak Ada berikan informasi ke user
            if(checkMapIdExist() == false){
                return;
            }

            // Store
            $.ajax({  
                url:"{{ url('/time_series/store')}}",  
                method:"POST",  
                data: {
                    'year' : $("#tahun").val(),
                    'map' : $('input[name="map[]"]').map(function(){ return this.value; }).get(),
                    'description' : $('input[name="description[]"]').map(function(){ return this.value; }).get(),
                    'jan' : $('input[name="jan[]"]').map(function(){ return this.value; }).get(),
                    'feb' : $('input[name="feb[]"]').map(function(){ return this.value; }).get(),
                    'mar' : $('input[name="mar[]"]').map(function(){ return this.value; }).get(),
                    'apr' : $('input[name="apr[]"]').map(function(){ return this.value; }).get(),
                    'may' : $('input[name="may[]"]').map(function(){ return this.value; }).get(),
                    'jun' : $('input[name="jun[]"]').map(function(){ return this.value; }).get(),
                    'jul' : $('input[name="jul[]"]').map(function(){ return this.value; }).get(),
                    'aug' : $('input[name="aug[]"]').map(function(){ return this.value; }).get(),
                    'sep' : $('input[name="sep[]"]').map(function(){ return this.value; }).get(),
                    'oct' : $('input[name="oct[]"]').map(function(){ return this.value; }).get(),
                    'nov' : $('input[name="nov[]"]').map(function(){ return this.value; }).get(),
                    'dec' : $('input[name="dec[]"]').map(function(){ return this.value; }).get()
                },
                type:'json',
                success : function(ajaxData){
                    $("#tbody_table_preview").empty();
                    $("#alert_messages_").text('Data Berhasil Disimpan');
                    $("#alert_").show();
                }
            });
        });
    });

    function checkMapIdExist(){
        var map = $('input[name="map[]"]').map(function(){ return this.value; }).get()
        map = Array.from(new Set(map))

        status = true
        $.ajax({
            url:"{{ url('/map/check_map_id_exist')}}",  
            method:"GET",
            data : {'map' : map},  
            type:'json',
            success : function(ajaxData){
                
                if(ajaxData.status == false){
                    var map_not_found = ajaxData.map_not_found.join()
                    $("#alert_messages_").text('Mata Anggaran Penerimaan ' + map_not_found + ' tidak terdaftar');
                    $("#alert_").show();

                    status = false;
                }
            }
        });

        return status;
    }
</script>
@endsection