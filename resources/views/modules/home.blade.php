@extends('master_template')

@section('konten')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0"><b>Home</b></h5>
                    </div>
                    <div class="card-body">
                        <p>Electronic Non Revenue (EL-NORA) Politeknik Negeri Jember
                            <ol>
                                <li>Pilih Menu MAP Untuk Mengelola Data Mata Anggaran Penerimaan</li>
                                <li>Pilih Menu Time Series > List Untuk Mengelola Data Time Series
                                    <ul>
                                        <li>Pilih Form Untuk Menginputkan Data Baru Dengan File berformat .xlsx</li>
                                        <li>Pilih List Data Untuk Mengelola Data Time Series Yang Tersimpan</li>
                                        <li>Pilih Plot Data Untuk Menampilkan Grafik</li>
                                    </ul>
                                </li>
                                <li>Pilih Menu Forecasting Untuk Melakukan Peramalan</li>
                                <li>Pilih Menu User Untuk Mengelola Data User</li>
                            </ol>
                        </p>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection