@extends('master_template')

@section('konten')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0"><b>Data Mata Anggaran Penerimaan</b></h5>
                        <div class="float-right">
                            <a href="{{ url('/') }}">Home</a> / MAP
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="alert_" class="alert alert-info alert-dismissible fade show" role="alert">
                            <b id="alert_messages_">&nbsp;</b>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <button type="button" id="btnAddData" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal_"><i class="fa fa-plus"></i> Tambah Data MAP</button>
                        <table class="table table-sm table-striped table-bordered" id="table_view">
                            <thead>
                                <tr>
                                    <td class="font-weight-bold text-center">No.</td>
                                    <td class="font-weight-bold text-center">Kode</td>
                                    <td class="font-weight-bold text-center">Nama MAP</td>
                                    <td class="font-weight-bold text-center">Aksi</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Modal -->
<div class="modal fade" id="modal_" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form name="form_input" method="POST">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel"><b>Tambah Data MAP</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <input type="hidden" name="id_" id="id_">
                    <div class="row">
                        <div class="col-md-6">
                            <small class="font-weight-bold">Kode MAP</small>
                            <input type="text" class="form-control form-control-sm" name="map_id" id="map_id" placeholder="Masukkan Kode MAP" />
                            <small id="kode_map_help_text" class="form-text text-danger"></small>
                        </div>
                        <div class="col-md-6">
                            <small class="font-weight-bold">Nama MAP</small>
                            <input type="text" class="form-control form-control-sm" name="map_name" id="map_name" placeholder="Masukkan Nama Kategori MAP" />
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSave" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('konten_js')
<script>
    //Initialization - Loaded First
    $(function () {
        loadDataTable();
        $("#alert_").hide();
    });
    
    //CSRF Token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Add New Data
    $('#btnAddData').on('click',function(event){
        $("#ModalLabel").addClass("font-weight-bold")
        $("#ModalLabel").html('Tambah Data MAP')
        emptyField();
    });

    //Save Data
    $('#btnSave').on('click', function(event) {
        event.preventDefault();

        //Check Kode Map, Must Have 6 Character
        if($("#map_id").val().length != 6 ){
            $("#kode_map_help_text").text("Kode Mata Anggaran Penerimaan harus 6 Karakter")
            return;
        }

        $.ajax({
            url : "{{ route('map.store') }}",
            method : "POST",
            data : {
                'id_' : $("#id_").val(),
                'map_id' : $("#map_id").val(),
                'map_name' : $("#map_name").val()
            },
            success : function(ajaxData){
                //Empty Field
                emptyField()
                //Hide Modal
                $("#modal_").modal('hide');
                //Modifikasi Alert                
                if(ajaxData == "1"){
                    $("#alert_messages_").text('Input Data Berhasil');
                }else{
                    $("#alert_messages_").text('Input Data Gagal');
                }
                $("#alert_").show();
                //Reload Table
                loadDataTable();
            }
        });
    });

    //Empty Field
    function emptyField(){
        $("#id_").val("")
        $("#map_id").val("")
        $("#map_name").val("")
    }

    //DataTable 
    function loadDataTable(){
        destroyDataTable();
        $("#table_view").DataTable({
            "responsive": true,
            "autoWidth": false,
            processing : true,
            serverSide : true,
            ajax : "{{ url('/map/data') }}",
            columns: [
                {render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {data : 'map_id'},
                {data : 'map_name'},
                { "className" : "text-center",
                    render: function (data, type, row, meta) {
                        var action_button = "<button class='btn btn-sm btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Aksi </button>"+
                                "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>"+
                                    "<a class='dropdown-item' href='#' onclick='edit("+row.map_id+")'>Edit</a>"+
                                    "<a class='dropdown-item' href='#' onclick='del("+row.map_id+")'>Delete</a>"+
                                "</div>";
                        return action_button;
                    }  
                },
            ],
        });
    }
    function destroyDataTable(){
        if ($.fn.DataTable.isDataTable('#table_view')) {
            $('#table_view').DataTable().destroy();
        }
        $('#table_view tbody').empty();
    }

    function edit(MapID){
        $("#modal_").modal('show');
        $("#ModalLabel").addClass("font-weight-bold")
        $("#ModalLabel").html('Edit Data MAP')
        $.ajax({
            type : "GET",
            url: "{{ url('map')}}"+"/"+MapID+"/"+"edit",
            success: function (ajaxData) {
                $("#id_").val(ajaxData.map_id)
                $("#map_id").val(ajaxData.map_id)
                $("#map_name").val(ajaxData.map_name)
            },
        });            
    }
    function del(MapID){
        if(confirm('Anda Yakin Menghapus Data?')){
            $.ajax({
                type : "DELETE",
                url: "{{ url('map')}}"+'/'+MapID,
                success: function (ajaxData) {
                    if(ajaxData == "1"){
                        $("#alert_messages_").text('Data Berhasil Dihapus');
                    }else{
                        $("#alert_messages_").text('Data Gagal Dihapus');
                    }
                    $("#alert_").show();
                    //Reload Table
                    loadDataTable();
                },
            });            
        }
    }
  </script>
@endsection