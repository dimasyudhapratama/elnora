@extends('master_template')

@section('konten')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0"><b>Forecasting</b></h5>
                        <div class="float-right">
                            <a href="{{ url('/') }}">Home</a> / Forecasting
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-lg-2">
                                <small class='font-weight-bold'>Data Acuan Awal</small>   
                                <select name="early_year" id="early_year" class="form-control form-control-sm">
                                    @foreach($year_list as $data)
                                    <option value="{{$data->year}}">{{$data->year}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <small class='font-weight-bold'>Data Acuan Akhir</small>   
                                <select name="final_year" id="final_year" class="form-control form-control-sm">
                                    @foreach($year_list as $data)
                                    <option value="{{$data->year}}">{{$data->year}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <small class='font-weight-bold'>MAP</small>   
                                <select name="map_id" id="map_id" class="form-control form-control-sm">
                                    <option value="ALL">ALL</option>
                                    @foreach($map_data as $map)
                                    <option value="{{$map->map_id}}">{{$map->map_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <small class='font-weight-bold'>Target</small>   
                                <select name="year_to_forecast" id="year_to_forecast" class="form-control form-control-sm">
                                    @php 
                                        $year_now = date('Y'); 
                                        $year_plus_4 = date('Y') + 4; 
                                    @endphp
                                    @for($year=$year_now;$year<=$year_plus_4;$year++)
                                    <option value="{{$year}}">{{$year}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <button type="button" id="btnCalculation" class="btn btn-primary btn-sm mb-2" onclick="calculateForecasts()"><i class="fa fa-calculator"></i>&nbsp;&nbsp;Kalkulasi</button>
                        <div class="chart">
                            <canvas id="plotChart" style="height:400px"></canvas>
                        </div>
                        <div class="mt-2">
                            <ol id="forecasts_desc_result">
                                <li>Gray Model
                                    <ul>
                                        <li id="gm_result_desc"></li>
                                        <li id="gm_mad"></li>
                                        <li id="gm_mape"></li>
                                    </ul>
                                </li>
                                <li>Least Square Model
                                <ul>
                                        <li id="lsm_result_desc"></li>
                                        <li id="lsm_mad"></li>
                                        <li id="lsm_mape"></li>
                                    </ul>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('konten_js')
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>

<script>
    //Initialization - Loaded First
    $(function () {
        $("#early_year option:last").attr("selected", "selected");
        $("#year_to_forecast option:last").attr("selected", "selected");


        $("#forecasts_desc_result").hide()
        $("#alert_").hide();
    });
    
    //CSRF Token - to prevent XSS Attack
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    

    //Table
    function calculateForecasts(){
        var early_year = parseInt($("#early_year").val())
        var final_year = parseInt($("#final_year").val())
        var map_id = $("#map_id").val()
        var year_to_forecast = parseInt($("#year_to_forecast").val())

        //Check selisih tahun
        //Jika selisih kurang dari 4 maka tidak diijinkan
        if((final_year - early_year) < 4){
            return alert("Jumlah data yang digunakan sebagai acuan minimal 4 tahun")
        }
        
        $.ajax({
            type : "GET",
            url : "{{ url('/forecasting/calculate_forecasts') }}",
            data : {
                early_year : early_year,
                final_year : final_year,
                map_id : map_id,
                year_to_forecast : year_to_forecast
            },
            success : function (data){
                console.log(data)
                var library_colors = ['#000','#7dccdb','#bf5063'];

                // //Looping Dataset
                var dataset = [];
                var data_recapitulation = data.data_recapitulation
                data_recapitulation.forEach(function(result, key){                    

                    var new_data = {
                            label: result.category_data_name, // Name the series
                            data: result.pnbp_income, // Specify the data values array
                            fill: false,
                            borderColor: library_colors[key], // Add custom color border (Line)
                            backgroundColor: library_colors[key], // Add custom color background (Points and Fill)
                            borderWidth: 1 // Specify bar border width
                    }            
                    dataset.push(new_data);
                });

                //Create Plot Chart
                createPlotChart(data.nameset,dataset)

                //Get Target Data
                // var gm_target = 

                //Count Error Rate
                var actual_array_data = data_recapitulation[0]['pnbp_income'];
                var gm_array_data = data_recapitulation[1]['pnbp_income'];
                var lsm_array_data = data_recapitulation[2]['pnbp_income'];

                countErrorRate(actual_array_data, gm_array_data, lsm_array_data)
            }
        });
    }

    function createPlotChart(nameset, dataset){
        var ctx = document.getElementById("plotChart").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: nameset,
                        datasets: dataset
                    },         
                    options: {
                    responsive: true, // Instruct chart js to respond nicely.
                    maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
                    }
                });
    }

    function countErrorRate(actual, gray_model, least_square_model){
        // Inisialisasi Variabel
        var n = actual.length

        var mad_gm_total = 0
        var mad_gm_average = 0

        var mad_lsm_total = 0
        var mad_lsm_average = 0

        var mape_gm_total = 0
        var mape_gm_average = 0

        var mape_lsm_total = 0
        var mape_lsm_average = 0

        var gm_target = 0
        var lsm_target = 0

        

        // Looping dihitung mulai index 1 (Data Kedua)
        // Karena nilai awal data actual, data gray model, dan data least square model sama

        for(var i=1; i<n; i++){
            //MAD Gray Model
            mad_gm_total += Math.abs(actual[i] - gray_model[i])

            //MAD Least Square Model
            mad_lsm_total += Math.abs(actual[i] - least_square_model[i])

            //MAPE Gray Model
            mape_gm_total += Math.abs(actual[i] - gray_model[i]) / actual[i]

            //MAPE Least Square Model
            mape_lsm_total += Math.abs(actual[i] - least_square_model[i]) / actual[i]

        }

        //Gray Model Last Data
        gm_last_index = gray_model.length - 1
        gm_target = gray_model[gm_last_index]
        $("#gm_result_desc").text("Data Pada Tahun "+ $("#year_to_forecast :selected").val() + " Sebesar Rp." + numberFormat(gm_target))

        //Calculation MAD Gray Model Average
        mad_gm_average = mad_gm_total / n
        $("#gm_mad").text("MAD : Rp." + numberFormat(mad_gm_average))

        //Calculation MAPE Gray Model Average
        mape_gm_average = mape_gm_total / n * 100 
        $("#gm_mape").text("MAPE : " + numberFormat(mape_gm_average) + "%")

        //Least Square Model Last Data
        lsm_last_index = least_square_model.length - 1
        lsm_target = least_square_model[lsm_last_index]
        $("#lsm_result_desc").text("Data Pada Tahun "+ $("#year_to_forecast :selected").val() + " Sebesar Rp." + numberFormat(lsm_target))
        
        //Calculation MAD Least Square Model Average
        mad_lsm_average = mad_lsm_total / n
        $("#lsm_mad").text("MAD : Rp." + numberFormat(mad_lsm_average))

        //Calculation MAPE Least Square Model Average
        mape_lsm_average = mape_lsm_total / n * 100
        $("#lsm_mape").text("MAPE : " + numberFormat(mape_lsm_average) + "%")

        //Show Forecast Description Result
        $("#forecasts_desc_result").show()
    }

    function numberFormat(bilangan){
        bilangan = bilangan.toFixed(3)
        bilangan = bilangan.replace (/\./g, ',');
        
        var	number_string = bilangan.toString(),
            split	= number_string.split(','),
            sisa 	= split[0].length % 3,
            angka 	= split[0].substr(0, sisa),
            ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                
        if (ribuan) {
            separator = sisa ? '.' : '';
            angka += separator + ribuan.join('.');
        }
        angka = split[1] != undefined ? angka + ',' + split[1] : angka;

        // Cetak hasil	
        return angka
    }
    
  </script>
@endsection