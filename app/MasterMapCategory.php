<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterMapCategory extends Model
{
    protected $table = 'master_map_category';
    protected $fillable = ['map_category_name'];
    
    public $timestamps = false;
}
