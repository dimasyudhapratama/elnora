<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterMap extends Model
{
    protected $table = 'master_map';
    protected $fillable = ['map_id','map_name'];
    
    public $timestamps = false;
}
