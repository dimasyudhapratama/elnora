<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSeriesMaster extends Model
{
    protected $table = 'time_series_master';
    protected $fillable = ['map_id','description','year'];
    
    public $timestamps = false;
}
