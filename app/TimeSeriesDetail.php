<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSeriesDetail extends Model
{
    protected $table = 'time_series_detail';
    protected $fillable = ['time_series_master_id','month','pnbp_income'];
    
    public $timestamps = false;
}
