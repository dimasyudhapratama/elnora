<?php

namespace App\Http\Controllers;

use App\MasterMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class MapController extends Controller{
    
    public function json(){
        return datatables()->of(MasterMap::get())->toJson();
    }

    public function index(){
        return view('modules/map/v_index');
    }
    
    public function store(Request $request){
        $id = $request->id_;

        //Map ID Must Unique
        if($id == ""){
            if(MasterMap::where('map_id', $request->map_id)->count() > 0){
                return "0";
            }
        }

        // Save/Update Operation
        $array_data = [
            'map_id' => $request->map_id,
            'map_name' => $request->map_name
        ];

        if($data = MasterMap::updateOrInsert(['map_id' => $id ], $array_data)){
            return "1";
        }else{
            return "0";
        }
    }
    
    public function edit($id){
        $where = array('map_id' => $id);
        $data  = MasterMap::where($where)->first();
 
        return Response::json($data);
    }
    
    public function destroy($id){
        $data = MasterMap::where('map_id',$id)->delete();
        return Response::json($data);
    }

    public function checkMapIdExist(Request $request){
        $map_id = $request->map;
        $status = true;
        $map_not_found = [];

        //Menghitung Jumlah Data
        for($i=0;$i<count($map_id);$i++){
            $count_of_data = MasterMap::where(['map_id' => $map_id[$i]])->count();
            if($count_of_data == 0){
                $status = false;
                array_push($map_not_found, $map_id[$i]);
            }
        }

        $return_data = [
            'status' => $status,
            'map_not_found' => $map_not_found
        ];

        return $return_data;


        
    }
}
