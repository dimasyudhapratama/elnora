<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ForecastingController extends Controller
{
    function index(){
        $map_data = DB::table('master_map')->get();
        $year_on_time_series_data = DB::table('time_series_master')->select('year')->distinct('year')->orderByDesc('year')->get();
        
        return view('modules/forecasting/v_index', ['map_data' => $map_data ,'year_list' => $year_on_time_series_data]);
    }

    function calculateForecasts(Request $request){
        //Get Data From Client
        $early_year = $request->early_year;
        $final_year = $request->final_year;
        $map_id = $request->map_id;
        $year_to_forecast = $request->year_to_forecast;

        //Initialize Variable
        $year = [];
        $actual = [];


        if($map_id == "ALL"){
            $data = DB::table('data_recapitulation_annual')
                    ->select('year',DB::raw('sum_annual as pnbp'))
                    ->whereBetween('year',[$early_year, $final_year])
                    ->get();
        }else{
            // $data = DB::table('data_recapitulation_annual')->where;
        }

        //Convert Std Class Object to Normal Array
        $data_arr = json_decode(json_encode($data),true);

        //Get PNBP Income Data
        foreach($data_arr as $record){
            array_push($actual, $record['pnbp']);
        }

        //Get Year for chart
        for($y=$data_arr['0']['year'];$y<=$year_to_forecast;$y++){
            array_push($year, $y);
        }

        $all_data = [
            'nameset' => $year,
            'data_recapitulation' => [
                [
                    'category_data_name' => 'Actual',
                    'pnbp_income' => $actual
                ],
                [
                    'category_data_name' => 'Forecasts : Gray Model ',
                    'pnbp_income' => $this->grayModel($year['0'], $year_to_forecast, $actual)
                ],
                [
                    'category_data_name' => 'Forecasts : Least Square',
                    'pnbp_income' => $this->leastSquareModel($year['0'], $year_to_forecast, $actual)
                ]
                
            ]
        ];

        return $all_data;
    }

    function grayModel($early_year, $year_to_forecast, $actual){
        
        $cumulative = []; //Array untuk menampung perhitungan cumulative
        $fitting = []; //Aray Untuk Menampung perhitungan Fitting
        $forecasts = []; //Array untuk menampung hasil peramalan


        $matrix_b_1 = []; //Matrix B
        $matrix_b_x_b_transpose = []; //Variabel untuk menampung Matrix B * Matrix B Transpose
        $matrix_b_x_b_transpose_inverse = []; //Variabel untuk menampung Invers(Matrix B * Matrix B Transpose)
        $matrix_b_x_b_transpose_inverse_transpose = [[],[]]; //Variabel untuk menampung Invers(Matrix B * Matrix B Transpose) * Matrix B Transpose
        
        // //Variabel untuk menampung hasil peramalan ke format data chart js
        // $gray_model_result = [];


        $a = 0;
        $b = 0;
        $c = 0;

        
        //Perulangan Untuk menemukan cumulative sequence, dan membuat matrix B
        for($index=0;$index<count($actual);$index++){
            //Data pertama cumulative sequence sama dengan Data PNBP
            if($index == 0){
                //Cumulative Sequence
                $cumulative[$index] = $actual[$index];
            }
            else{
                //Cumulative Sequence
                $cumulative[$index] = $actual[$index] + $cumulative[$index-1];
                
                //Create Matrix B - Matrix Awal
                //Kolom pertama didapatkan dengan rumus : -0.5 *(Nilai Kumulatif sebelumnya + Nilai Kumulatif Sekarang)
                $row_matrix_b_1[0] = -0.5 * ($cumulative[$index-1] + $cumulative[$index]);
                $row_matrix_b_1[1] = 1;
                
                //Push data ke $matrix_b_1
                array_push($matrix_b_1,$row_matrix_b_1);
            }
        }

        //Perkalian Matrix : Matrix B Transpose * Matrix B
        $row_0_0 = 0;
        $row_0_1 = 0;
        $row_1_0 = 0;
        $row_1_1 = 0;
        for($index=0;$index<count($matrix_b_1);$index++){
            $row_0_0 += $matrix_b_1[$index][0] * $matrix_b_1[$index][0];
            $row_0_1 += $matrix_b_1[$index][0] * $matrix_b_1[$index][1];
            $row_1_0 += $matrix_b_1[$index][1] * $matrix_b_1[$index][0];
            $row_1_1 += $matrix_b_1[$index][1] * $matrix_b_1[$index][1];
        }
        $matrix_b_x_b_transpose = [
            [$row_0_0 , $row_0_1],
            [$row_1_0, $row_1_1]
        ];


        //Invers(Matrix B Transpose * Matrix B)
        $determinan = $row_0_0 * $row_1_1 - $row_0_1 * $row_1_0;
        $row_0_0 = (1/$determinan) * $matrix_b_x_b_transpose[1][1];
        $row_0_1 = (1/$determinan) * -$matrix_b_x_b_transpose[0][1];
        $row_1_0 = (1/$determinan) * -$matrix_b_x_b_transpose[1][0];
        $row_1_1 = (1/$determinan) * $matrix_b_x_b_transpose[0][0];
        $matrix_b_x_b_transpose_inverse = [
            [$row_0_0 , $row_0_1],
            [$row_1_0, $row_1_1]
        ];

        
        //Invers(Matrix B Transpose * Matrix B) * Matrix B Transpose
        for($index=0;$index<count($matrix_b_x_b_transpose_inverse);$index++){
            $val_0 = $matrix_b_x_b_transpose_inverse[$index][0];
            $val_1 = $matrix_b_x_b_transpose_inverse[$index][1];
            
            for($index2=0;$index2<count($matrix_b_1);$index2++){
                $val2_0 = $matrix_b_1[$index2][0];
                $val2_1 = $matrix_b_1[$index2][1];

                $val = $val_0 * $val2_0 + $val_1 * $val2_1;

                array_push($matrix_b_x_b_transpose_inverse_transpose[$index],$val);
            }
            
        }

        //Mencari Nilai A, B, C
        for($index=1;$index<count($actual);$index++){
            //Menghitung a
            $a += $actual[$index] * $matrix_b_x_b_transpose_inverse_transpose[0][$index-1];
            //Menghitung b
            $b += $actual[$index] * $matrix_b_x_b_transpose_inverse_transpose[1][$index-1];

        }
        //Menghitung C
        $c = $b/$a;
        

        //Mencari Nilai fitting sequence, dan forecast
        $index = 0;
        for($year=$early_year;$year<=$year_to_forecast;$year++){
            //Data pertama Fitting sequence dan forecast sama dengan Data PNBP
            if($index == 0){
                //Fitting Sequence
                array_push($fitting, $actual[$index]);
                
                //Forecast Sequence
                array_push($forecasts, $actual[$index]);
            }else{
                //Fitting Sequence
                $fitting_value = ($actual[0] - $c)*exp(-$a * $index) + $c;
                array_push($fitting, $fitting_value);

                //Forecast Sequence
                $forecasts_value = $fitting[$index] - $fitting[$index-1];
                array_push($forecasts, $forecasts_value);
            }

            $index++;
        }

        return $forecasts;

    }

    function leastSquareModel($early_year, $year_to_forecast, $actual){

        //Inisialisasi Variabel
        $y = $actual;
        $n = count($y);
        $sum_y = array_sum($y);
        $sum_x = 0;
        $sum_xy = 0;
        $sum_x_kuadrat = 0;
        $a = 0;
        $b = 0;
        $least_square_model_result = [];

        //Kalkulasi untuk mendapatkan x, xy dan x^2
        for($index=0;$index<$n;$index++){
            //Menghitung nilai x
            if($index == 0){
                if($n%2 == 0){//Jika Jumlah Data Genap
                    $x_value = 1 - $n;
                    
                }else{ //Jika Jumlah Data Ganjil
                    $x_value = -(($n/2) - 0.5);
                }
            }else{
                if($n%2 == 0){//Jika Jumlah Data Genap
                    $x_value += 2;
                    
                }else{ //Jika Jumlah Data Ganjil
                    $x_value += 1;
                }
            } 

            //Tambahkan ke variabel sum x
            // $sum_x += $x_value;

            //Mencari nilai XY
            $xy_value =  $x_value * $y[$index];
            
            //Tambahkan ke variabel sum xy
            $sum_xy += $xy_value;

            //mencari nilai x kuadrat
            $x_kuadrat_value = pow($x_value,2);
            $sum_x_kuadrat += $x_kuadrat_value;

        }

        //Mencari Nilai A dan Nilai B
        $a = $sum_y / $n;
        $b = $sum_xy / $sum_x_kuadrat;

        $periode = 1;
        for($year=$early_year;$year<=$year_to_forecast;$year++){
            if($periode == 1){
                array_push($least_square_model_result, $y[0]);
            }else{
                $forecasts_value = $a + $b * $periode;
                array_push($least_square_model_result, $forecasts_value);
            }

            $periode++;
        }

        return $least_square_model_result;
    }
}
