<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GrayModelForecastingController extends Controller
{
    function index(){
        //Initialization
        $year_to_forecast = 2022;
        $matrix_b_1 = []; //Matrix B
        $matrix_b_x_b_transpose = []; //Variabel untuk menampung Matrix B * Matrix B Transpose
        $matrix_b_x_b_transpose_inverse = []; //Variabel untuk menampung Invers(Matrix B * Matrix B Transpose)
        $matrix_b_x_b_transpose_inverse_transpose = [[],[]]; //Variabel untuk menampung Invers(Matrix B * Matrix B Transpose) * Matrix B Transpose


        $a = 0;
        $b = 0;
        $c = 0;

        //Data
        $time_series = [
            ["year" => 2015, "pnbp" =>40924829827],
            ["year" => 2016, "pnbp" =>50787924250],
            ["year" => 2017, "pnbp" =>53521774100],
            ["year" => 2018, "pnbp" =>57803636773],
            ["year" => 2019, "pnbp" =>62399404446],
            ["year" => 2020, "pnbp" =>63224422000],
        ];

        //Perulangan Untuk menemukan cumulative sequence, dan membuat matrix B
        for($index=0;$index<count($time_series);$index++){
            //Data pertama cumulative sequence sama dengan Data PNBP
            if($index == 0){
                //Cumulative Sequence
                $time_series[$index]['cumulative'] = $time_series[$index]['pnbp'];
            }
            else{
                //Cumulative Sequence
                $time_series[$index]['cumulative'] = $time_series[$index]['pnbp'] + $time_series[$index-1]['cumulative'];
                
                //Create Matrix B - Matrix Awal
                //Kolom pertama didapatkan dengan rumus : -0.5 *(Nilai Kumulatif sebelumnya + Nilai Kumulatif Sekarang)
                $row_matrix_b_1[0] = -0.5 * ($time_series[$index-1]['cumulative'] + $time_series[$index]['cumulative']);
                $row_matrix_b_1[1] = 1;
                
                //Push data ke $matrix_b_1
                array_push($matrix_b_1,$row_matrix_b_1);
            }
        }

        //Perkalian Matrix : Matrix B Transpose * Matrix B
        $row_0_0 = 0;
        $row_0_1 = 0;
        $row_1_0 = 0;
        $row_1_1 = 0;
        for($index=0;$index<count($matrix_b_1);$index++){
            $row_0_0 += $matrix_b_1[$index][0] * $matrix_b_1[$index][0];
            $row_0_1 += $matrix_b_1[$index][0] * $matrix_b_1[$index][1];
            $row_1_0 += $matrix_b_1[$index][1] * $matrix_b_1[$index][0];
            $row_1_1 += $matrix_b_1[$index][1] * $matrix_b_1[$index][1];
        }
        $matrix_b_x_b_transpose = [
            [$row_0_0 , $row_0_1],
            [$row_1_0, $row_1_1]
        ];


        //Invers(Matrix B Transpose * Matrix B)
        $determinan = $row_0_0 * $row_1_1 - $row_0_1 * $row_1_0;
        $row_0_0 = (1/$determinan) * $matrix_b_x_b_transpose[1][1];
        $row_0_1 = (1/$determinan) * -$matrix_b_x_b_transpose[0][1];
        $row_1_0 = (1/$determinan) * -$matrix_b_x_b_transpose[1][0];
        $row_1_1 = (1/$determinan) * $matrix_b_x_b_transpose[0][0];
        $matrix_b_x_b_transpose_inverse = [
            [$row_0_0 , $row_0_1],
            [$row_1_0, $row_1_1]
        ];

        
        //Invers(Matrix B Transpose * Matrix B) * Matrix B Transpose
        for($index=0;$index<count($matrix_b_x_b_transpose_inverse);$index++){
            $val_0 = $matrix_b_x_b_transpose_inverse[$index][0];
            $val_1 = $matrix_b_x_b_transpose_inverse[$index][1];
            
            for($index2=0;$index2<count($matrix_b_1);$index2++){
                $val2_0 = $matrix_b_1[$index2][0];
                $val2_1 = $matrix_b_1[$index2][1];

                $val = $val_0 * $val2_0 + $val_1 * $val2_1;

                array_push($matrix_b_x_b_transpose_inverse_transpose[$index],$val);
            }
            
        }

        //Mencari Nilai A, B, C
        for($index=1;$index<count($time_series);$index++){
            //Menghitung a
            $a += $time_series[$index]['pnbp'] * $matrix_b_x_b_transpose_inverse_transpose[0][$index-1];
            //Menghitung b
            $b += $time_series[$index]['pnbp'] * $matrix_b_x_b_transpose_inverse_transpose[1][$index-1];

        }
        //Menghitung C
        $c = $b/$a;
        

        //Mencari Nilai fitting sequence, dan forecast
        $index = 0;
        for($year=$time_series[0]['year'];$year<=$year_to_forecast;$year++){
            //Data pertama Fitting sequence dan forecast sama dengan Data PNBP
            if($index == 0){
                //Fitting Sequence
                $time_series[$index]['fitting'] = $time_series[$index]['pnbp'];
                //Forecast Sequence
                $time_series[$index]['forecast'] = $time_series[$index]['pnbp'];
            }else{
                //Fitting Sequence
                $time_series[$index]['fitting'] = ($time_series[0]['pnbp'] - $c)*exp(-$a * $index) + $c;                
                //Forecast Sequence
                $time_series[$index]['forecast'] = $time_series[$index]['fitting'] - $time_series[$index-1]['fitting'];

                //Mengecek index tahun, jika tidak ada maka diinisialisasi
                if(!array_key_exists('year',$time_series[$index])){
                    $time_series[$index]['year'] = $year;
                }
            }
            $index++;
        }    


        // echo "<pre>";
        // print_r($time_series);
        // print_r($matrix_b_1);
        // print_r($matrix_b_x_b_transpose);
        // print_r($matrix_b_x_b_transpose_inverse);
        // print_r($matrix_b_x_b_transpose_inverse_transpose);
        // print_r($a);
        // echo "<br>";
        // print_r($b);
        // echo "<br>";
        // print_r($c);
        // echo "<br>";
        // echo "</pre>";

        return json_encode($time_series);


    }
}
