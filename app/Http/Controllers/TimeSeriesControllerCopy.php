<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DB;

//PHPSpreadsheet
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

//Time Series Model
use App\MasterMapCategory;
use App\MasterMap;
use App\TimeSeriesMaster;
use App\TimeSeriesDetail;

class TimeSeriesControllerCopy extends Controller
{
    //Form Input Data Time Series
    function form(){
        return view('modules/time_series/input_time_series');
    }

    function downloadFormat(){
        $file = public_path(). "/download/format_input.xlsx";
        return response()->download($file);
    }

    function loadExcel(Request $request){
        $validation = Validator::make($request->all(), [
            'excel_file' => 'required|mimes:xls,xlsx|max:2048'
        ]);
        if($validation->passes()){
            $file = $request->file('excel_file');
            $new_file_name = rand() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploaded'), $new_file_name);
            // Read Excel File
            $arr_file = explode('.', $new_file_name);
            $extension = end($arr_file);

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        
            // $spreadsheet = $reader->load(public_path('uploaded/'.$new_file_name));
            $spreadsheet = $reader->load(public_path('uploaded/format_input.xlsx'));
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            $data = array();
            for($i = 4;$i < count($sheetData);$i++){                
                //Add Record Array
                $row = array(
                    'map' => $sheetData[$i][0],
                    'keterangan' => $sheetData[$i][1],
                    'jan' => $sheetData[$i][2],
                    'feb' => $sheetData[$i][3],
                    'mar' => $sheetData[$i][4],
                    'apr' => $sheetData[$i][5],
                    'mei' => $sheetData[$i][6],
                    'jun' => $sheetData[$i][7],
                    'jul' => $sheetData[$i][8],
                    'agu' => $sheetData[$i][9],
                    'sep' => $sheetData[$i][10],
                    'okt' => $sheetData[$i][11],
                    'nov' => $sheetData[$i][12],
                    'des' => $sheetData[$i][13]
                );
                $data[] = $row;
            }

            return response()->json([
                'tahun' => $sheetData[2][1],
                'data_uploaded' => $data
            ]);
            
            Storage::delete('uploaded/'.$new_file_name);

            

        }else{
            return response()->json([
                'message'   => $validation->errors()->all(),
                'uploaded_file' => ''
            ]);
        }
    }

    function store(Request $request){
        //Data Tahun
        $year = $request->year;

        //Loop Seluruh Data
        for($i=0; $i<count($request->map); $i++){
            //Data MAP ID dan Keterangan
            $map = $request->map[$i];
            $description = $request->description[$i];

            $where = [
                'map_id' => $map,
                'year' => $year,
            ];

            $data = [
                'map_id' => $map,
                'description' => $description,
                'year' => $year,
            ];
            $timeSeriesMaster = TimeSeriesMaster::updateOrCreate($where,$data);

            //Loop Pendapatan PNBP Per Bulan
            for($j=0; $j<12;$j++){
                $month = $j + 1; //Index dimulai dari 0, Sedangkan Bulan dimulai dari 1. Maka untuk memudahkan, digunakan index + 1
                $pnbp_income = $request->pnbp_income[$j][$i];

                // //Parameter Pertama sebagai Where, Parameter Kedua sebagai data yang diinsert/update
                $where = [
                    'time_series_master_id' => $timeSeriesMaster->id,
                    'month' => $month, 
                ];

                $data = [
                    'map_id' => $map,
                    'time_series_master_id' => $timeSeriesMaster->id,
                    'month' => $month,
                    'pnbp_income' => $pnbp_income
                ];

                TimeSeriesDetail::updateOrCreate($where,$data);
            }
        }
    }

    function allTimeSeriesData(){
        $year_on_time_series_data = TimeSeriesMaster::select('year')->distinct('year')->orderByDesc('year')->get();
        return view('modules/time_series/list_time_series',['year_list' => $year_on_time_series_data]);
    }

    function annualData($year){
        //All Data
        $all_data = array();

        //Get Data From Map Category Table
        $map_category = MasterMapCategory::get();
        
        $data = array();
        foreach($map_category as $map_category_data){
            $data['map_category_name'] = $map_category_data->map_category_name;
            
            //Get Time Series Master Data From DB
            $time_series_master = TimeSeriesMaster::select('time_series_master.id','map_id','description')
                                    ->join('master_map','time_series_master.map_id','=','master_map.id')
                                    ->join('master_map_category','master_map.map_category_id','=','master_map_category.id')
                                    ->where('master_map_category.id',$map_category_data->id)
                                    ->where('year',$year);
            
            $data['time_series_master'] = array();

            foreach($time_series_master->get() as $record){
                $master['id'] = $record->id;
                $master['map_id'] = $record->map_id;
                $master['description'] = $record->description;

                //Time Series Detail
                $time_series_detail = TimeSeriesDetail::select('month','pnbp_income')
                                        ->join('time_series_master','time_series_detail.time_series_master_id','=','time_series_master.id')
                                        ->join('master_map','time_series_master.map_id','=','master_map.id')
                                        ->join('master_map_category','master_map.map_category_id','=','master_map_category.id')
                                        ->where('year',$year)
                                        ->where('time_series_master.id',$record->id)
                                        ->orderBy('month')
                                        ->get();
                
                $master['time_series_detail'] = array();                                                       
                foreach($time_series_detail as $record){
                    $master['time_series_detail'][$record->month] = $record->pnbp_income;
                }


                //Push Array
                array_push($data['time_series_master'],$master);
                
            }
            
            array_push($all_data,$data);
        }

        return view('modules/time_series/annual_time_series_table',['all_data' => $all_data]);
    }

    function edit(Request $request){
        $year = $request->year;
        $id = $request->id;

        $time_series_detail = TimeSeriesDetail::select('time_series_detail.id','pnbp_income')
        ->join('time_series_master','time_series_detail.time_series_master_id','=','time_series_master.id')
        ->join('master_map','time_series_master.map_id','=','master_map.id')
        ->join('master_map_category','master_map.map_category_id','=','master_map_category.id')
        ->where('year',$year)
        ->where('time_series_master.id',$id)
        ->orderBy('month')
        ->get();

        $time_series_detail[0]['month_in_indo'] = "Januari";
        $time_series_detail[1]['month_in_indo'] = "Februari";
        $time_series_detail[2]['month_in_indo'] = "Maret";
        $time_series_detail[3]['month_in_indo'] = "April";
        $time_series_detail[4]['month_in_indo'] = "Mei";
        $time_series_detail[5]['month_in_indo'] = "Juni";
        $time_series_detail[6]['month_in_indo'] = "Juli";
        $time_series_detail[7]['month_in_indo'] = "Agustus";
        $time_series_detail[8]['month_in_indo'] = "September";
        $time_series_detail[9]['month_in_indo'] = "Oktober";
        $time_series_detail[10]['month_in_indo'] = "November";
        $time_series_detail[11]['month_in_indo'] = "Desember";
        
        return response()->json($time_series_detail);
    }

    

    

   
}
