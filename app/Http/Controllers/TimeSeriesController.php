<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use Validator;
use Response;
use DB;

//PHPSpreadsheet
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

//Time Series Model
use App\MasterMap;
use App\TimeSeriesMaster;
use App\DataRecapitulationAnnual;

class TimeSeriesController extends Controller
{
    //Form Input Data Time Series
    function form(){
        return view('modules/time_series/input_time_series');
    }

    function downloadFormat(){
        $file = public_path(). "/download/format_input.xlsx";
        return response()->download($file);
    }

    function loadExcel(Request $request){
        $validation = Validator::make($request->all(), [
            'excel_file' => 'required|mimes:xls,xlsx|max:2048'
        ]);
        if($validation->passes()){
            $file = $request->file('excel_file');
            $new_file_name = rand() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploaded'), $new_file_name);
            // Read Excel File
            $arr_file = explode('.', $new_file_name);
            $extension = end($arr_file);

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        
            $spreadsheet = $reader->load(public_path('uploaded/'.$new_file_name));
            // $spreadsheet = $reader->load(public_path('uploaded/format_input.xlsx'));
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            $data = array();
            for($i = 4;$i < count($sheetData);$i++){                
                //Add Record Array
                $row = array(
                    'map' => $sheetData[$i][0],
                    'keterangan' => $sheetData[$i][1],
                    'jan' => $sheetData[$i][2],
                    'feb' => $sheetData[$i][3],
                    'mar' => $sheetData[$i][4],
                    'apr' => $sheetData[$i][5],
                    'may' => $sheetData[$i][6],
                    'jun' => $sheetData[$i][7],
                    'jul' => $sheetData[$i][8],
                    'aug' => $sheetData[$i][9],
                    'sep' => $sheetData[$i][10],
                    'oct' => $sheetData[$i][11],
                    'nov' => $sheetData[$i][12],
                    'dec' => $sheetData[$i][13]
                );
                $data[] = $row;
            }

            return response()->json([
                'tahun' => $sheetData[2][1],
                'data_uploaded' => $data,
                'filename' => $new_file_name
            ]);
            
            // Storage::delete('uploaded/'.$new_file_name);
            unlink(public_path(). "/uploaded/".$new_file_name);           

        }else{
            return response()->json([
                'message'   => $validation->errors()->all(),
                'uploaded_file' => ''
            ]);
        }
    }

    function store(Request $request){
        //Data Tahun
        $year = $request->year;

        //Loop Seluruh Data
        for($i=0; $i<count($request->map); $i++){
            $data = [
                'map_id' => $request->map[$i],
                'year' => $year,
                'description' => $request->description[$i],
                'jan' => $request->jan[$i],
                'feb' => $request->feb[$i],
                'mar' => $request->mar[$i],
                'apr' => $request->apr[$i],
                'may' => $request->may[$i],
                'jun' => $request->jun[$i],
                'jul' => $request->jul[$i],
                'aug' => $request->aug[$i],
                'sep' => $request->sep[$i],
                'oct' => $request->oct[$i],
                'nov' => $request->nov[$i],
                'dec' => $request->dec[$i],

            ];
            $timeSeriesMaster = TimeSeriesMaster::insert($data);
        }
    }

    function allTimeSeriesData(){
        $map_data = MasterMap::get();
        $year_on_time_series_data = TimeSeriesMaster::select('year')->distinct('year')->orderByDesc('year')->get();
        return view('modules/time_series/list_time_series',['map_data' => $map_data , 'year_list' => $year_on_time_series_data]);
    }

    function annualData($year){
        //All Data
        $all_data = array();

        //Get Data From Map Table
        $map = MasterMap::get();
        
        $data = array();
        foreach($map as $map_data){
            $data['map_id'] = $map_data->map_id;
            $data['map_name'] = $map_data->map_name;

            $time_series_data = TimeSeriesMaster::select('id','description','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec')
                                ->where('year',$year)->where('map_id',$map_data->map_id)->get();
            $data['time_series_master'] = json_decode(json_encode($time_series_data), true);
            
            array_push($all_data,$data);
        }

        return view('modules/time_series/annual_time_series_table',['all_data' => $all_data]);
    }

    function edit(Request $request){
        $id = $request->id;
        
        $time_series_detail = TimeSeriesMaster::where('id',$id)->first();
        
        return $time_series_detail;
    }

    function update(Request $request){        
        //Update Data Master
        $data = [
            'map_id' => $request->map_id,
            'year' => $request->year,
            'description' => $request->description,
            'jan' => $request->jan,
            'feb' => $request->feb,
            'mar' => $request->mar,
            'apr' => $request->apr,
            'may' => $request->may,
            'jun' => $request->jun,
            'jul' => $request->jul,
            'aug' => $request->aug,
            'sep' => $request->sep,
            'oct' => $request->oct,
            'nov' => $request->nov,
            'dec' => $request->dec,
        ];
        $update = TimeSeriesMaster::where('id',$request->id)->update($data);

        if($update){
            return "1";
        }else{
            return "0";
        }
    }

    function delete(Request $request){
        $id = $request->id_;
        $delete = TimeSeriesMaster::find($id)->delete();

        if($delete){
            return "1";
        }else{
            return "0";
        }
    }

    //Plot Data
    function plotData(){
        $year_on_time_series_data = TimeSeriesMaster::select('year')->distinct('year')->orderByDesc('year')->get();
        return view('modules/time_series/plot_time_series',['year_list' => $year_on_time_series_data]);
    }

    function getPlotData(Request $request){
        $tipe = $request->tipe;
        $early_year = $request->early_year;
        $final_year = $request->final_year;
        
        //Struktur Array untuk return ke Frontend
        $all_data = [
            'nameset' => [],
            'data_recapitulation' => []
        ];

        //Query Rekapitulasi All Data 
        if($tipe == "monthly"){
            $data_recapitulation = DB::table('data_recapitulation_monthly')->get();
        }else if($tipe == "annual"){
            $data_recapitulation = DB::table('data_recapitulation_annual')->get();            
        }

        //Convert std object to array
        $data_recapitulation_arr = json_decode(json_encode($data_recapitulation),true);

        $data['category_name'] = 'Semua Data';
        $data['pnbp_income'] = [];

        //Looping Data dan mengambil label serta record untuk data keseluruhan
        foreach($data_recapitulation_arr as $record){
            //Mengambil Data Untuk Title
            //Jika Tipe Bulanan, Maka Title adalah 'bulan/tahun'. 
            //jika Tipe Tahunan, Maka title adalah 'tahun'
            if($tipe == "monthly"){ 
                // Loop Bulan 1 - 12
                for($i=1;$i<=12;$i++){
                    //Nameset
                    array_push($all_data['nameset'],$i.'/'.substr($record['year'],2));
                    array_push($data['pnbp_income'], $record['sum_'.$i]);    
                }

            }else{
                //Nameset
                array_push($all_data['nameset'],$record['year']);
                //PNBP Income
                array_push($data['pnbp_income'], $record['sum_annual']);
            }

            
        }
        array_push($all_data['data_recapitulation'],$data);


        // //Rekapitulasi Per Kategori
        $map = MasterMap::get();
        foreach($map as $map_data){
            $data['category_name'] = $map_data->map_name;
            $data['title'] = [];
            $data['pnbp_income'] = [];

            //Query Rekapitulasi Data Per Kategori
            if($tipe == "monthly"){
                $data_recapitulation = DB::table('data_recapitulation_monthly_map_grouped')->where('map_id',$map_data->map_id)->get();
            }else if($tipe == "annual"){
                $data_recapitulation = DB::table('data_recapitulation_annual_map_grouped')->where('map_id',$map_data->map_id)->get();
            }

            //Convert std object to array
            $data_recapitulation_arr = json_decode(json_encode($data_recapitulation),true);

            //Looping Data
            foreach($data_recapitulation_arr as $record){                
                if($tipe == "monthly"){ 
                    // Loop Bulan 1 - 12
                    for($i=1;$i<=12;$i++){
                        //Nameset
                        array_push($data['pnbp_income'], $record['sum_'.$i]);    
                    }    
                }else{
                    //PNBP Income
                    array_push($data['pnbp_income'], $record['sum_annual']);
                }
            }
            array_push($all_data['data_recapitulation'],$data);
        }

        return $all_data;
    }
   
}
